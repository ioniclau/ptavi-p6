#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""

import socketserver
import sys
import simplertp
import secrets


class EchoHandler(socketserver.DatagramRequestHandler):
    """
    Echo server class
    """

    def handle(self):
        # Escribe dirección y puerto del cliente (de tupla client_address)

        texto = self.rfile.read().decode('utf-8')
        print(texto)
        metodo = texto.split(" ")[0]
        if metodo in ["INVITE", "ACK", "BYE"]:
            if texto.split(" ")[1].split(":")[0] != "sip":
                self.wfile.write(b"SIP/2.0 400 Bad Request\r\n")

            else:

                if metodo == 'INVITE':
                    resp = b"SIP/2.0 100 Trying\r\n\r\n"
                    resp1 = (resp + b"SIP/2.0 180 Ringing\r\n\r\n")
                    resp2 = (resp1 + b"SIP/2.0 200 OK \r\n\r\n")
                    self.wfile.write(resp2)

                elif metodo == 'ACK':
                    BIT = secrets.randbelow(1)
                    RTP_header = simplertp.RtpHeader()
                    RTP_header.set_header(version=2, marker=BIT,
                                          payload_type=14, ssrc=200002)
                    audio = simplertp.RtpPayloadMp3(mp3)
                    simplertp.send_rtp_packet(RTP_header, audio,
                                              direccip, 23032)

                if metodo == "BYE":
                    self.wfile.write(b"SIP/2.0 200 OK\r\n")

        else:
            self.wfile.write(b"SIP/2.0 405 Method Not Allowed\r\n")


if __name__ == "__main__":

    # Creamos servidor de eco y escuchamos
    puerto = int(sys.argv[2])
    direccip = sys.argv[1]
    mp3 = sys.argv[3]
    serv = socketserver.UDPServer((direccip, puerto), EchoHandler)
    print("Listening...")
    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        sys.exit("Finalizando el servidor")
