#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Programa cliente que abre un socket a un servidor
"""

import socket
import sys

try:

    metodo = sys.argv[1]
    usuario = sys.argv[2]
    lista = usuario.split(":")
    usuario_sip = lista[0]
    direccip = usuario_sip.split("@")[1]
    puerto = int(lista[1])
    LINE = metodo + ' sip:' + usuario_sip + ' SIP/2.0\r\n\r\n'

except (IndexError, ValueError):
    sys.exit("Usage: client.py method receiver@IP:SIPport")

LINE2 = "v=0 \r\no=papijuancho@gmail.com " + direccip + \
        "\r\ns=papisesion \r\nt=0 \r\nm=audio " + str(puerto) + " RTP"
long = len(LINE2)
LINE3 = LINE + "Content-Lenght: " + str(long) + '\r\n' + LINE2

# Creamos el socket, lo configuramos y lo atamos a un servidor/puerto
with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
    my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    my_socket.connect((direccip, puerto))

    print("Enviando: ", LINE)
    if metodo != "INVITE":
        my_socket.send(bytes(LINE, 'utf-8'))
    elif metodo == "INVITE":
        my_socket.send(bytes(LINE3, 'utf-8'))
    data = my_socket.recv(1024)

    print('Recibido -- ', data.decode('utf-8'))

    if metodo == 'INVITE':
        datoserver = data.decode('utf-8')
        resp = "SIP/2.0 200 OK "
        datoserver2 = datoserver.split("\r\n")[4]

    if datoserver2 == resp:
        asent = 'ACK' + ' sip:' + usuario_sip + ' SIP/2.0\r\n\r\n'
        my_socket.send(bytes(asent, 'utf-8'))

print("Terminando socket...")

print("Fin.")
